require "spec_helper"
require File.expand_path('../../lib/engine.rb', __FILE__)
require 'set'
require 'readline'

describe "Wimdu Property Register Engine" do

	describe "ask engine" do

		it "should generate a valid id" do
			engine = RegisterEngine.new
			id = engine.generate_id
			expect(id.length).to eq(8)
		end

		it "should be able to start registering a new property" do
			double_p = double("property_double")
			allow(Property).to receive(:new).with(any_args).and_return(double_p)
			allow(double_p).to receive(:id).and_return("test")

			engine = RegisterEngine.new

			allow(engine).to receive(:register)
			expect(engine).to receive(:register).with(double_p)
			expect(engine).to receive(:message).with(kind_of(String))

			engine.new_property
		end

		it "should generate random ids" do
			engine = RegisterEngine.new

			id_set = Set.new

			(1..1000).each do
				id_set.add(engine.generate_id())
			end

			expect(id_set.length).to eq(1000)
		end

		it "register: should iterate on properties and call ask. after its done, present congrats message" do
			PROPERTY_ORDER_FAKE = [{"field1" => "message1"}, {"field2" => "message2"}]

			stub_const("RegisterEngine::PROPERTY_ORDER", PROPERTY_ORDER_FAKE)

			double_p = double("property")

			engine = RegisterEngine.new

			expect(engine).to receive(:ask).with(double_p, "field1", "message1")
			expect(engine).to receive(:ask).with(double_p, "field2", "message2")
			expect(engine).to receive(:message).with(kind_of(String))

			expect(double_p).to receive(:is_complete?).and_return(true)
			expect(double_p).to receive(:id).and_return("test-id")

			engine.register(double_p)
		end

		it "ask: should not ask the user for input if the attribute is already written" do

			double_p = double("property")

			expect(double_p).to receive(:send).with(any_args).and_return("im here")

			engine = RegisterEngine.new

			result = engine.ask(double_p, "anything", "test-message")

			expect(result).to be true
		end

		it "ask: should ask the user for input if attribute is not present" do
			double_p = double("property")
			attribute = "test_attribute"
			user_input = "user input"
			message = "message"

			expect(double_p).to receive(:send).with("#{attribute}").and_return(nil)
			expect(double_p).to receive(:send).with("#{attribute}=", user_input)
			expect(double_p).to receive(:save) # should save after every ask

			engine = RegisterEngine.new
			expect(engine).to receive(:get_input).with(message).and_return(user_input)

			engine.ask(double_p, attribute, message)
		end

		it "ask: should ask the user again if the first time raises an error" do
			double_p = double("property")
			attribute = "test_attribute"
			user_input = "user input"
			message = "message"

			expect(double_p).to receive(:send).with("#{attribute}").and_return(nil).twice
			
			expect(double_p).to receive(:send).with("#{attribute}=", user_input).ordered.and_raise(ArgumentError, "message")
			expect(double_p).to receive(:send).with("#{attribute}=", user_input).ordered.and_return(true)
			
			expect(double_p).to receive(:save).once 

			engine = RegisterEngine.new
			expect(engine).to receive(:get_input).with(message).and_return(user_input).twice

			expect(STDOUT).to receive(:puts)

			engine.ask(double_p, attribute, message)
		end

		it "message: Should message the user at screen" do
			expect(STDOUT).to receive(:puts).with("message")

			engine = RegisterEngine.new
			engine.message "message"
		end

		it "get_input: should ask for input using rb-readline" do
			message = "Will Bruno be an excellent hire? [Y/n]:"

			expect(Readline).to receive(:readline).with(message + " ")

			engine = RegisterEngine.new
			engine.get_input(message)
		end

		it "should be able to continue to register a property" do
			property = Property.new("1", :incomplete)
			expect(Property).to receive(:get).with("1").and_return(property)

			engine = RegisterEngine.new

			expect(engine).to receive(:register).with(property)
			expect(STDOUT).to receive(:puts)

			engine.continue("1")
		end
	end


end


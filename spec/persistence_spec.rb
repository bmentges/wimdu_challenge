require "spec_helper"
require File.expand_path('../../lib/persistence.rb', __FILE__)
require File.expand_path('../../lib/models.rb', __FILE__)
require "sqlite3"

describe "Wimdu Persistence" do

	describe "setup persistence" do

		it "should have a db_path defined" do
			expect(WimduPersistence.class_variables).to include(:@@db_path)
		end

		it "should have a init_sql_path defined" do
			expect(WimduPersistence.class_variables).to include(:@@init_sql_path) 
		end

		it "should create a database if database is not present at initialization" do
		
			# mocking the world :( binds to implementation but
			# since its a file operation, its ok.	
			db = double("database")

			expect(File).to receive(:exists?)
				.with(any_args)
				.and_return(false)

			expect(SQLite3::Database).to receive(:new)
				.and_return(db)

			expect(IO).to receive(:read)
				.with(any_args)
				.and_return(:sql)

			expect(db).to receive(:execute).with(:sql)

			# BDD - not testing specifics of the initialize,
			# just behaviour
			WimduPersistence.new	
		end

		it "should not create a database if it alread exists" do

			expect(File).to receive(:exists?)
				.with(any_args)
				.and_return(true)

			expect(SQLite3::Database).to_not receive(:new)
			expect(IO).to_not receive(:read)

			WimduPersistence.new
		end
	end

	describe "SQL functionality" do

		it "should get a connection" do
			db = double("database")

			expect(File).to receive(:exists?)
				.with(any_args)
				.and_return(true)
			expect(SQLite3::Database).to receive(:open)
				.and_return(db)

			wimduPersistence = WimduPersistence.new
			conn = wimduPersistence.get_connection

			expect(conn).to eq(db)
		end

		it "should be able to save a property" do
			db = double("database")

			expect(File).to receive(:exists?)
				.with(any_args)
				.and_return(true)
			expect(SQLite3::Database).to receive(:open)
				.and_return(db)

			wimduPersistence = WimduPersistence.new

			p = Property.new("1234")
			p.title = :test
			p.type = :apartment
			p.address = :test
			p.rate = "15"
			p.max_guests = "15"
			p.email = :test
			p.phone = :test

			expect(db).to receive(:execute)
				.with(any_args)
				.and_return(true)

			r = wimduPersistence.save(p)

			expect(r).to be true
		end

		it "should reject saving anything but properties" do

			expect(File).to receive(:exists?)
				.with(any_args)
				.and_return(true)

			wimduPersistence = WimduPersistence.new

			expect { wimduPersistence.save(123) }.to raise_error(ArgumentError)
	
		end

		it "should be able to list properties that are complete" do
			expect(File).to receive(:exists?)
				.with(any_args)
				.and_return(true)

			wimduPersistence = WimduPersistence.new

			db = double("db_connection")

			expect(wimduPersistence).to receive(:get_connection).and_return(db)
			expect(db).to receive(:execute).with(/status = 'complete'/)

			wimduPersistence.list
		end

		it "should be able to find a property by id" do
			expect(File).to receive(:exists?)
				.with(any_args)
				.and_return(true)

			wimduPersistence = WimduPersistence.new

			db = double("db_connection")

			expect(wimduPersistence).to receive(:get_connection).and_return(db)
			expect(db).to receive(:execute).with(/id = '1'/)

			wimduPersistence.get("1")
		end
	end
end

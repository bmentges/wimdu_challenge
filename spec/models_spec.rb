require "spec_helper"
require File.expand_path('../../lib/models.rb', __FILE__)

describe "Wimdu Property Model" do
	describe "attributes are present" do
		it "should be able to set up a title" do
			property = Property.new
			property.title = :test

			expect(property.title).to eq(:test)
		end

		it "should be able to set a property title of the correct type" do
			property = Property.new
			
			property.type = :holiday_home
			expect(property.type).to eq(:holiday_home)
			property.type = "holiday_home"
			expect(property.type).to eq(:holiday_home)


			property.type = :apartment
			expect(property.type).to eq(:apartment)
			property.type = "apartment"
			expect(property.type).to eq(:apartment)


			property.type = :private_room
			expect(property.type).to eq(:private_room)
			property.type = "private_room"
			expect(property.type).to eq(:private_room)
		end

		it "should be able to set an address" do
			address = "Voltastr. 5, 13355 Berlin"
			property = Property.new
			property.address = address		
			expect(property.address).to eq(address)
		end

		it "should be able to set a rate" do
			property = Property.new
			property.rate = "12"
			expect(property.rate).to eq(12)
			property.rate = "150"
			expect(property.rate).to eq(150)
		end

		it "should be able to set a number of max guests" do
			property = Property.new
			property.max_guests = "5"
			expect(property.max_guests).to eq(5)
		end

		it "should be able to set an email" do
			property = Property.new
			property.email = "bmentges@gmail.com"
			expect(property.email).to eq("bmentges@gmail.com")
		end

		it "should be able to set a phone" do
			real_brunos_phone = "+55 21 965480027"
			property = Property.new
			property.phone = real_brunos_phone
			expect(property.phone).to eq(real_brunos_phone)
		end

		it "should not be able to change status from the outside" do 
			property = Property.new
			expect(property.methods).to_not include(:status=)
		end

		it "should be able to read the status" do
			property = Property.new
			expect(property.methods).to include(:status)
		end

		it "should not be able to change the id from the outside" do 
			property = Property.new
			expect(property.methods).to_not include(:id=)
		end

		it "should be able to read the id" do
			property = Property.new
			expect(property.methods).to include(:id)
		end


		it "should raise an error if its an invalid type" do
			property = Property.new
			
			expect { property.type = :hotel}.to raise_error(ArgumentError)
			
		end

		it "should raise an error if its an invalid rate" do
			property = Property.new

			expect { property.rate = "asdf" }.to raise_error(ArgumentError)
			expect { property.rate = "123zz" }.to raise_error(ArgumentError)
			expect { property.rate = "$10.00" }.to raise_error(ArgumentError)

		end

		it "should raise an error if its an invalid max guests" do
			property = Property.new

			expect { property.max_guests = "definitely wrong" }.to raise_error(ArgumentError)
			expect { property.max_guests = "12abc" }.to raise_error(ArgumentError)
			expect { property.max_guests = "$10" }.to raise_error(ArgumentError)
		end

		it "should set an id if passed in the constructor" do
			p = Property.new 1
			expect(p.id).to eq(1)
		end

		it "should initialize with :incomplete status" do
			p = Property.new
			expect(p.status).to eq(:incomplete)
		end

		it "should not be complete if all fields are empty" do
			p = Property.new
			expect(p.is_complete?).to be false
		end

		it "should not be compelte if some fields are empty" do
			p = Property.new
			p.title = "Bruno is heading to Wimdu! yay!"
			p.type = :apartment

			expect(p.is_complete?).to be false
		end

		it "should be complete if all fields are filled" do
			p = Property.new("123")

			p.title = "I hope you guys like my code!"
			p.type = :apartment
			p.address = "R. Olivedos, 81, Sobrado, Rio de Janeiro"
			p.rate = "100"
			p.max_guests = "5" # you can come to Brazil :)
			p.email = "bmentges@gmail.com"
			p.phone = "+55 21 965480027"

			expect(p.is_complete?).to be true
		end

	end
	describe "save functionality" do

		it "should be able to save itself" do
			p = Property.new("123")
			
			persistence = double("wimdu")
			expect(WimduPersistence).to receive(:new)
				.and_return(persistence)
			
			expect(persistence).to receive(:save)
					.with(kind_of(Property)).and_return(true)

			expect(p.save).to be true
		end

		it "should return false if we try to save and sqlite sends us an exception" do
			p = Property.new("123")
			
			persistence = double("wimdu")
			expect(WimduPersistence).to receive(:new)
				.and_return(persistence)
			
			expect(persistence).to receive(:save)
				.with(kind_of(Property)).and_raise(Exception, "Something wrong with the SQL")

			expect(STDOUT).to receive(:puts).with(any_args)
			
			expect(p.save).to be false
		end

	end

	describe "list functionality" do
		it "should be able to list properties" do
			db = double("persistence")
			results = [ 
				["1", "2", "apartment", "4", "5", "6", "7", "8", "complete"]
			]

			expect(WimduPersistence).to receive(:new).and_return(db)
			expect(db).to receive(:list).and_return(results)

			got_it = Property.list

			expect(got_it.length).to eq(1)

			property = got_it.first

			expect(property).to be_instance_of(Property)

			expect(property.id).to eq("1")
			expect(property.title).to eq("2")
			expect(property.type).to eq(:apartment)
			expect(property.address).to eq("4")
			expect(property.rate).to eq(5)
			expect(property.max_guests).to eq(6)
			expect(property.email).to eq("7")
			expect(property.phone).to eq("8")
			expect(property.status).to eq(:complete)
		end

		it "should return empty list in case of exception" do
			db = double("persistence")

			expect(WimduPersistence).to receive(:new).and_return(db)
			expect(db).to receive(:list).and_raise(Exception, "something bad happened...")
			expect(STDOUT).to receive(:puts).with(any_args)

			got_it = Property.list

			expect(got_it).to eq([])
		end
	end

	describe "continue functionality" do
		it "should return a property instance when asked for an existent id" do
			db = double("persistence")

			results = [ 
				["1", "2", "apartment", "4", "5", "6", "7", "8", "complete"]
			]
			expect(WimduPersistence).to receive(:new).and_return(db)
			expect(db).to receive(:get).with("1").and_return(results)

			property = Property.get("1")

			expect(property).to be_instance_of(Property)

			expect(property.id).to eq("1")
			expect(property.title).to eq("2")
			expect(property.type).to eq(:apartment)
			expect(property.address).to eq("4")
			expect(property.rate).to eq(5)
			expect(property.max_guests).to eq(6)
			expect(property.email).to eq("7")
			expect(property.phone).to eq("8")
			expect(property.status).to eq(:complete)
		end

		it "should return nil if its not found" do
			db = double("persistence")

			results = []
			expect(WimduPersistence).to receive(:new).and_return(db)
			expect(db).to receive(:get).with("1").and_return(results)

			property = Property.get("1")

			expect(property).to be nil
		end

		it "should return nil if something bad happens" do
			db = double("persistence")

			expect(WimduPersistence).to receive(:new).and_return(db)
			expect(db).to receive(:get).and_raise(Exception, "something bad happened...")
			expect(STDOUT).to receive(:puts).with(any_args)

			property = Property.get("1")

			expect(property).to be nil
		end
	end
end

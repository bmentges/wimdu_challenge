require "spec_helper"

describe "Wimdu CLI" do
  let(:exe) { File.expand_path('../../bin/wimdu', __FILE__) }

  describe "new" do
    let(:cmd) { "#{exe} new" }

    it "allows for entering data" do
      process = run_interactive(cmd)
      expect(process.output).to include("Starting with new property")
      expect(process.output).to include("Title: ")
      type "My Title"
      expect(process.output).to include("Address: ")
      type "My Address"
      expect(process.output).to include("Property Type ")
      type "apartment"
      expect(process.output).to include("Nightly rate in EUR: ")
      type "12"
      expect(process.output).to include("Max guests: ")
      type "5"
      expect(process.output).to include("Email: ")
      type "bmentges@gmail.com"
      expect(process.output).to include("Phone: ")
      type "+55 21 96548-0027"
      expect(process.output).to include("Great job!")
    end
  end
end




require File.expand_path("../persistence.rb", __FILE__)

class Property
	attr_accessor :title, :type, :address, :rate, :max_guests, :email, :phone
	attr_reader :status, :id

	@@valid_types = [:holiday_home, :apartment, :private_room]

	def initialize(id=nil, status=:incomplete)
		@title = @type = @address = @rate = @max_guests = @email = @phone = nil
		@id = id 
		@status = status
	end

	def type=(value)
		raise ArgumentError, "Error: The type of a property can only be: holiday_home, apartment or private_room" unless (@@valid_types.include? value.to_sym or value.nil?)

		@type = value.to_sym
	end

	def rate=(value)
		raise ArgumentError, "Error: rate must be a number. Examples: 12 or 150" unless self.is_number?(value) 

		@rate = value.to_i
	end

	def is_number?(value)
		return value.to_i.to_s == value 	
	end

	def max_guests=(value)
		raise ArgumentError, "Error: max_guests must be a number. Example: 12 or 5" unless self.is_number?(value) 

		@max_guests = value.to_i
	end

	def is_complete?
		# My eyes also bleed with that much repetition
		# i'll probably refactor and add the nil.empty? from 
		# rails... 
		# But this applies a monadic way, elegant IMO. 
		# (The Maybe Monad, purists might say its an 
		# Applicative Functor... ok)
		# Sometimes it's better to keep it understandable
		# than to make it a one liner
		
		result = @title.to_s.strip.length > 0
		result &&= @type.to_s.strip.length > 0
		result &&= @address.to_s.strip.length > 0
		result &&= @rate.to_s.strip.length > 0
		result &&= @max_guests.to_s.strip.length > 0
		result &&= @email.to_s.strip.length > 0
		result &&= @phone.to_s.strip.length > 0
		result &&= @id.to_s.strip.length > 0

		result
	end

	def save
		@status = :complete if self.is_complete?

		wimdu_p = WimduPersistence.new
		begin
			wimdu_p.save(self)
			true
		rescue Exception => e
			puts "Error saving this Property: #{e.message}"
			false
		end
	end

	def self.list
		wimdu_p = WimduPersistence.new
		begin
			list = wimdu_p.list
			
			properties = []
			
			list.each { |r| 
				p = self.extract_property_from_sql_result(r)
				properties.push(p)
			}

			return properties
		rescue Exception => e
			puts "Error listing properties: #{e.message}"
			[]
		end
	end

	def self.get(id)
		return nil if id.nil? 

		wimdu_p = WimduPersistence.new

		begin
			result = wimdu_p.get(id)

			if (result.length==1) 
				self.extract_property_from_sql_result(result.first)
			else
				nil
			end
		rescue Exception => exc
			puts "Error getting a Property. #{exc.message}"
			nil
		end
	end

	def self.extract_property_from_sql_result(sql_result)
		id = sql_result[0]
		title = sql_result[1] 
		type = sql_result[2] 
		address = sql_result[3] 
		rate = sql_result[4]
		max_guests = sql_result[5]
		email = sql_result[6] 
		phone = sql_result[7] 
		status = sql_result[8].to_sym 

		p = Property.new(id, status)
		p.title = title unless self.check_nil(title)
		p.type = type unless self.check_nil(type)
		p.address = address unless self.check_nil(address)
		p.rate = rate.to_s unless self.check_nil(rate)
		p.max_guests = max_guests.to_s unless self.check_nil(max_guests)
		p.email = email unless self.check_nil(email)
		p.phone = phone unless self.check_nil(phone)
		
		p
	end

	def self.check_nil(item)
		item == "" || item == 0
	end
end

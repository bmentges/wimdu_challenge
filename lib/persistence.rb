require "sqlite3"

class WimduPersistence

	@@db_path = File.expand_path("../../conf/.database.sqlite3", __FILE__)
	@@init_sql_path = File.expand_path("../../conf/init.sql", __FILE__)

	def initialize
		self.setup_persistence
	end

	def setup_persistence
		unless File.exists?(@@db_path)
			db = SQLite3::Database.new @@db_path 
			sql = IO.read(@@init_sql_path)
			db.execute(sql)
		end
	end

	def get_connection
		SQLite3::Database.open @@db_path
	end

	def save(property)

		raise ArgumentError, "Cannot save anything but a Property" unless property.instance_of? Property

		save_query = "INSERT or REPLACE into properties (id, title, property_type, address, rate, max_guests, email, phone, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?)"

		db = self.get_connection
		result = db.execute(save_query,
							 property.id.to_s,
							 property.title.to_s,
							 property.type.to_s,
							 property.address.to_s,
							 property.rate.to_i,
							 property.max_guests.to_i,
							 property.email.to_s,
							 property.phone.to_s,
							 property.status.to_s)
		result
	end

	def list
		list_query = "SELECT * from properties where status = 'complete'"

		db = self.get_connection
		result = db.execute(list_query)
		result
	end

	def get(id)
		get_query = "SELECT * from properties where id = '#{id}'"

		db = self.get_connection
		result = db.execute(get_query)
		result
	end
end



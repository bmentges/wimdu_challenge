require File.expand_path('../models.rb', __FILE__)
require 'readline'

class RegisterEngine

	PROPERTY_ORDER = [
		{:title => "Title:"}, 
		{:address => "Address:"},
		{:type => "Property Type [apartment, holiday_home or private_room]:"},
		{:rate => "Nightly rate in EUR:"},
		{:max_guests => "Max guests:"},
		{:email => "Email:"},
		{:phone => "Phone:"}
	]


	def new_property
		property = Property.new(self.generate_id)

		welcome = "Starting with new property #{property.id}."
		self.message(welcome)

		self.register(property)
	end

	def continue(id)
		property = Property.get(id)
		
		raise Exception, "Property not found." if property.nil?
		raise Exception, "Property already complete." if property.is_complete?

		puts "Continuing with #{id}.\n\n"
		register(property)
	end

	def generate_id
		rand.to_s[2..9]
		# need to guarantee later that its unique... 
	end

	def register(property)
		PROPERTY_ORDER.each { |field| 
			key = field.keys.first
			value = field[key]
			self.ask(property, key, value)
		}

		congrats = "Great job! Listing #{property.id} is complete!"
		self.message(congrats) if property.is_complete?
	end

	def ask(property, attribute, message)
		old_attr_value = property.send(attribute)
		return true unless old_attr_value.nil? || old_attr_value == 0

		value = self.get_input(message)

		begin
			property.send("#{attribute}=", value)
			property.save()
		rescue ArgumentError => e
			self.message e.message
			self.ask(property, attribute, message)
		end
	end

	def message(message)
		puts message
	end

	def get_input(message)
		Readline::readline("#{message} ")
	end
end

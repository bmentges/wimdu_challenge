CREATE TABLE properties (
	id TEXT PRIMARY KEY NOT NULL,
	title TEXT NULL,
	property_type TEXT NULL,
	address TEXT NULL,
	rate INTEGER NULL,
	max_guests INTEGER NULL,
	email TEXT NULL,
	phone TEXT NULL,
	status TEXT NULL
);